package io.adjofun.restricted;

import io.adjofun.restricted.exception.RestrictedException;

import java.util.function.Predicate;

public class Restricted<T> {
    private Predicate<T> predicate;
    private T value;

    private Restricted(Predicate<T> predicate) {
        this.predicate = predicate;
    }

    public static <T> Restricted<T> with(Predicate<T> predicate) {
        if (predicate == null) throw new IllegalArgumentException("Predicate can't be null");
        return new Restricted<T>(predicate);
    }

    public boolean check(T val) {
        return this.predicate.test(val);
    }

    public boolean checkStored() {
        return this.predicate.test(this.value);
    }

    public T getUnsafe() {
        if (this.value == null) throw new RestrictedException.Unchecked("No stored value");
        if (!this.predicate.test(this.value)) {
            String exMsg = String.format("Stored value %s is no longer restricted", this.value);
            throw new RestrictedException.Unchecked(exMsg);
        }
        return this.value;
    }

    public T get() throws RestrictedException {
        if (this.value == null) throw new RestrictedException("No stored value");
        if (!this.predicate.test(this.value)) {
            String exMsg = String.format("Stored value %s is no longer restricted", this.value);
            throw new RestrictedException(exMsg);
        }
        return this.value;
    }

    public Restricted<T> putUnsafe(T val) {
        if (val == null) throw new RestrictedException.Unchecked("Restricted value can't be null");
        if (!this.predicate.test(val)) {
            String exMsg = String.format("Value %s is not restricted", val);
            throw new RestrictedException.Unchecked(exMsg);
        }
        this.value = val;
        return this;
    }

    public Restricted<T> put(T val) throws RestrictedException {
        if (val == null) throw new RestrictedException("Restricted value can't be null");
        if (!this.predicate.test(val)) {
            String exMsg = String.format("Value %s is not restricted", val);
            throw new RestrictedException(exMsg);
        }
        this.value = val;
        return this;
    }
}
