package io.adjofun.restricted.exception;

public class RestrictedException extends Exception {

    public static class Unchecked extends RuntimeException {
        public Unchecked() { super(); }
        public Unchecked(String errMsg) { super(errMsg); }
        public Unchecked(Throwable t) { super(t); }
    }

    public RestrictedException() { super(); }
    public RestrictedException(String errMsg) { super(errMsg); }
    public RestrictedException(Throwable t) { super(t); }
}
