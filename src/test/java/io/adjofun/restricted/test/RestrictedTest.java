package io.adjofun.restricted.test;

import io.adjofun.restricted.Restricted;
import org.junit.Assert;
import org.junit.Test;

public class RestrictedTest {

    @Test
    public void putGet() throws Exception {
        Restricted<Integer> positive = Restricted.with(i -> i > 0);
        Assert.assertEquals((Integer)42, positive.put(42).get());
    }

    @Test
    public void putPut() throws Exception {
        Restricted<String> str = Restricted.with(s -> s.length() == 4);
        Assert.assertEquals("buzz", str.put("fizz").put("buzz").get());
    }

    @Test
    public void getGet() throws Exception {
        Restricted<Integer> b = Restricted.with(Character::isDefined);
        b.put(31);
        Assert.assertEquals(b.get(), b.get());
    }
}
